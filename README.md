# LPCrux #

A [Lakitu](https://bitbucket.org/maccosslab/lakitu) pipeline to perform [Crux](http://crux.ms/) searches of mass spectrometry data.

## Testing

```bash
lpcrux/interface.py --workers 100 MainTask --run-id smoke_test --ms-set s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/msfiles.txt --msconvert-config s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/msconvert_config.crux.txt --fasta-file s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/uniprot-human_no_tr.fasta --parameter-file s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/jarrett_high_high.params.txt
```