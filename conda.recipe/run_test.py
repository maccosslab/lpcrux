"""
conda build recognizes test failure when run_test.py returns a non-zero exit code
"""
import os
import subprocess
import sys

os.environ['LAKITU_AWS_REGION'] = 'us-east-1'
ret_code = subprocess.call(["python", "-m", "luigi", "--module", "lpcrux.interface", "MainTask", "--local-scheduler", "--help"])
sys.exit(ret_code)
