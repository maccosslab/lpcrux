from lpmsconvert.abstract import MSConvertBase
from luigi.util import inherits

from abstract import RunId
from buildtooldirectory import BuildToolDirectory


@inherits(RunId)
class MSConvert(MSConvertBase):
    def requires(self):
        reqs = {"msconvert_dir": self.clone(BuildToolDirectory, toolname="msconvert")}
        return reqs

    @property
    def downloadable_outputs(self):
        return []
