import luigi
from luigi.util import inherits

from abstract import MSConvertParams, CometParams
from abstract import PercolatorBase
from abstract import RunId
from buildtooldirectory import BuildToolDirectory
from makepin import MakePin


@inherits(RunId)
@inherits(MSConvertParams)
@inherits(CometParams)
class Percolator(PercolatorBase):
    ms_files = luigi.ListParameter()

    @property
    def tool_wd(self):
        return self.input()['tool_wd'].path

    @property
    def pin_in(self):
        return self.input()['in_make_pin']['out_pin'].path

    def requires(self):
        return {
            'tool_wd': self.clone(BuildToolDirectory, toolname='percolator'),
            'in_make_pin': self.clone(MakePin, ms_files=self.ms_files)
        }
