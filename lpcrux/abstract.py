import posixpath
from datetime import datetime

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws.batch import BatchTask
from lakitu.aws.s3helpers import bucket_and_key_from_s3, bs3
from lakituapi.pipeline import LakituAwsConfig
from luigi.parameter import ParameterException
from luigi.util import inherits


def read_from_s3(s3_path):
    """
    Read a file from an S3 source.

    A string of bytes is returned. To read and iterate over a text file, for example, you could do this:

    >>> import StringIO
    >>> msg = read_from_s3('s3://bucket-name/key/foo.txt')
    >>> buf = StringIO.StringIO(msg)
    >>> for line in buf.readline():
    >>>     pass # Do stuff here

    :param s3_path: Path starting with s3://, e.g. 's3://bucket-name/key/foo.txt'
    :return: content : bytes
    """
    bucket, key = bucket_and_key_from_s3(s3_path)
    s3_object = bs3.get_object(Bucket=bucket, Key=key)
    body = s3_object['Body']
    return body.read()


def get_s3_file_size(s3_path):
    """
    Returns the size in bytes of an s3 object

    :param s3_path: Path starting with s3://, e.g. 's3://bucket-name/key/foo.txt'
    :return: size of file in bytes
    """
    bucket, key = bucket_and_key_from_s3(s3_path)
    response = bs3.head_object(Bucket=bucket, Key=key)
    size = response['ContentLength']
    return size


def get_s3_line_count(s3_path):
    """
    Returns the line count of a tab-separated S3 object
    :param s3_path: Path of tab-separated S3 object starting with s3://, e.g. 's3://bucket-name/key/foo.tsv'
    :return int: line count
    """
    bucket, key = bucket_and_key_from_s3(s3_path)
    response = bs3.head_object(Bucket=bucket, Key=key)
    r = bs3.select_object_content(
        Bucket=bucket,
        Key=key,
        ExpressionType='SQL',
        Expression="select count(*) from s3object s",
        InputSerialization={'CSV': {"FileHeaderInfo": "IGNORE", "FieldDelimiter": "\\011"}},
        OutputSerialization={'CSV': {}},
    )

    for event in r['Payload']:
        if 'Records' in event:
            records = event['Records']['Payload'].decode('utf-8')
            return int(records)
    raise Exception("Failed reading line count of s3 object {}".format(s3_path))


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


class SpecificTaskParameter(luigi.Parameter):
    """
    Parameter corresponding to a specific instantiated task object
    """

    def serialize(self, x):
        return x.task_id


class S3PathParameter(luigi.Parameter):
    """
    Reads in and validates an s3 path
    """

    def parse(self, x):
        if x is None:
            return None
        if not x.startswith("s3://"):
            raise ParameterException("Path must be a valid s3 path or list of s3 paths")
        return x


class RunId(luigi.Config):
    run_id = luigi.Parameter(default=datetime.now().strftime('%Y-%m-%d_%H-%M-%S'),
                             description="A unique identifier for storing data, including intermediate files and final"
                                         " output.")


class CometParams(luigi.Config):
    fasta_file = S3PathParameter(description='Comet: S3 path to FASTA file')
    parameter_file = S3PathParameter(description='Comet: S3 path to Crux parameter file')
    file_prefix = luigi.Parameter(default='',
                                  description='Comet: Prefix to add to each filename.')


class MSConvertParams(luigi.Config):
    msconvert_config = S3PathParameter(description="S3 path to msconvert config")


@inherits(CometParams)
class CometBase(BatchTask):
    @property
    def tool_wd(self):
        """
        :return: S3 path of directory where output can be stored
        """
        raise NotImplementedError

    @property
    def fasta_in(self):
        """
        :return: S3 path of input fasta file
        """
        raise NotImplementedError

    @property
    def mzxml_in(self):
        """
        :return: S3 path of input mzxml file
        """
        raise NotImplementedError

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn
    job_def = {
        'jobDefinitionName': 'crux_3_2_comet',
        'type': 'container',
        'containerProperties':
            {
                'image': 'atkeller/crux:3.2',
                'memory': 16000,
                'vcpus': 36  # allocate at least one core for this
            }
    }

    # /BatchTask Interface

    # AWSTaskBase Interface
    @property
    def command_base(self):
        cmd = ['comet',
               '--parameter-file', 'Ref::parameter_file',
               '--output-dir', 'Ref::out_dir']
        if self.file_prefix:
            cmd += ['--fileroot', 'Ref::file_prefix']
        cmd += ['--output_sqtfile', '1',
                '--output_percolatorfile', '1',
                'Ref::mzxml_in',
                'Ref::fasta_in']
        return cmd

    @property
    def environment(self):
        # get the s3 path of the output directory:
        #  e.g. s3://bucket_name/crux_dir/out_dir -> bucket_name/crux_dir/out_dir
        out_path = remove_prefix(self.tool_wd, prefix='s3://')
        additional_uploads = ""
        additional_uploads += posixpath.join(out_path, 'comet.target.pin')
        additional_uploads += ':' + posixpath.join(out_path, 'comet.target.sqt')
        additional_uploads += ':' + posixpath.join(out_path, 'comet.decoy.sqt')
        additional_uploads += ':' + posixpath.join(out_path, 'comet.params.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'comet.log.txt')
        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads}]

    @property
    def parameters(self):
        return {
            'file_prefix': str(self.file_prefix),
            'parameter_file': self.parameter_file,
            'mzxml_in': self.mzxml_in,
            'fasta_in': self.fasta_in,
            'out_dir': self.tool_wd
        }

    # /AWSTaskBase Interface

    # Luigi Interface
    def output(self):
        out_dir = self.tool_wd
        return {'target_sqt': s3.S3Target(posixpath.join(out_dir, 'comet.target.sqt')),
                'decoy_sqt': s3.S3Target(posixpath.join(out_dir, 'comet.decoy.sqt')),
                'comet_params_txt': s3.S3Target(posixpath.join(out_dir, 'comet.params.txt')),
                'comet_target_pin': s3.S3Target(posixpath.join(out_dir, 'comet.target.pin')),
                'comet_log_txt': s3.S3Target(posixpath.join(out_dir, 'comet.log.txt')),
                }
    # /Luigi Interface


class MakePinBase(BatchTask):
    @property
    def tool_wd(self):
        """
        :return: S3 path of directory where output can be stored
        """
        raise NotImplementedError

    @property
    def out_pin(self):
        """
        :return: S3 path of output pin file
        """
        raise NotImplementedError

    @property
    def in_pins(self):
        """
        :return: An iterable of S3 paths for each input pin file
        """
        raise NotImplementedError

    @property
    def estimated_memory_requirement(self):
        """
        Estimates the memory requirements of this job based on the input files.
        :return: memory requirement in MiB
        """
        minimum_memory = 2048
        maximum_memory = 1952 * 1024  # The largest EC2 instance available has 1952 GiB memory
        estimated_memory = int(max([get_s3_file_size(pin) / (1024 * 1024) for pin in self.in_pins]) * 1.5)
        if estimated_memory > maximum_memory:
            raise Exception(
                "Memory estimate to run MakePin exceeds maximum available ({} GiB)".format(maximum_memory / 1024))
        return max(estimated_memory, minimum_memory)

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn

    @property
    def job_def(self):
        return {
            'jobDefinitionName': 'crux_pipeline_0_3_make_pin',
            'type': 'container',
            'containerProperties':
                {
                    'image': 'atkeller/crux-pipeline:0.3',
                    'memory': self.estimated_memory_requirement,
                    'vcpus': 2  # allocate at least one core for this
                }
        }

    # /BatchTask Interface

    # AWSTaskBase Interface
    @property
    def command_base(self):
        cmd = ['make-pin']
        cmd += [pin for pin in self.in_pins]
        cmd += ['Ref::out_pin']
        return cmd

    @property
    def environment(self):
        # get the s3 path of the output directory:
        #  e.g. s3://bucket_name/crux_dir/out_dir -> bucket_name/crux_dir/out_dir
        out_path = remove_prefix(self.tool_wd, prefix='s3://')
        additional_uploads = ""

        additional_downloads = ""
        additional_downloads += out_path + '/'

        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads},
                {'name': 's3wrap_extra_downloads', 'value': additional_downloads}]

    @property
    def parameters(self):
        return {
            'out_pin': self.out_pin
        }

    # /AWSTaskBase Interface

    # Luigi Interface
    def output(self):
        out_dir = self.tool_wd
        return {'out_pin': s3.S3Target(posixpath.join(out_dir, 'make-pin.pin'))}
    # /Luigi Interface


class PercolatorBase(BatchTask):
    @property
    def tool_wd(self):
        raise NotImplementedError

    @property
    def pin_in(self):
        raise NotImplementedError

    def requires(self):
        raise NotImplementedError

    @property
    def use_heuristic_subset_max_train(self):
        return False

    @property
    def estimated_memory_requirement(self):
        """
        Estimates the memory requirements of this job based on the input files.
        :return: memory requirement in MiB
        """
        minimum_memory = 2048
        maximum_memory = 1952 * 1024  # The largest EC2 instance available has 1952 GiB memory
        # Based on a test using a 15.9 GiB PIN file, Percolator reads the PIN file into memory and consumes just
        # under 2.5x the file size. Just after reading, an additional 0.5x of the file size is used just before cross
        # validation begins. Data were collected using Cloudwatch Logs to graph ECS Memory Utilization.
        estimated_memory = int((get_s3_file_size(self.pin_in) / (1024 * 1024)) * 5.0)
        if estimated_memory > maximum_memory:
            raise Exception(
                "Memory estimate to run Percolator exceeds maximum available ({} GiB)".format(maximum_memory / 1024))
        return max(estimated_memory, minimum_memory)

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn

    @property
    def job_def(self):
        return {
            'jobDefinitionName': 'crux_3_2_percolator',
            'type': 'container',
            'containerProperties':
                {
                    'image': 'atkeller/crux:3.2',
                    'memory': self.estimated_memory_requirement,
                    'vcpus': 2  # Percolator doesn't yet support multiprocessing, so adding CPUs won't increase performance
                }
        }
    # /BatchTask Interface

    # AWSTaskBase Interface
    @property
    def command_base(self):
        cmd = ['percolator',
               '--parameter-file', 'Ref::parameter_file',
               '--output-dir', 'Ref::out_dir']
        if float(get_s3_file_size(self.pin_in)) / float(1024 * 1024 * 1024) > 0.33 and self.use_heuristic_subset_max_train:
            # Subset when >1 million PSMs. Use 0.33 GiB as a heuristic for ~1 million PSMs
            cmd += ['--subset-max-train', '500000']
        else:
            cmd += ['--subset-max-train', '1000000']
        if self.file_prefix:
            cmd += ['--fileroot', 'Ref::file_prefix']
        cmd += ['--pout-output', 'T',
                'Ref::pin_in']
        return cmd

    @property
    def environment(self):
        # get the s3 path of the output directory:
        #  e.g. s3://bucket_name/crux_dir/out_dir -> bucket_name/crux_dir/out_dir
        out_path = remove_prefix(self.tool_wd, prefix='s3://')
        additional_uploads = ""
        additional_uploads += posixpath.join(out_path, 'percolator.target.proteins.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.decoy.proteins.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.target.peptides.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.decoy.peptides.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.target.psms.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.decoy.psms.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.params.txt')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.pep.xml')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.pout.xml')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.mzid')
        additional_uploads += ':' + posixpath.join(out_path, 'percolator.log.txt')
        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads}]

    @property
    def parameters(self):
        return {
            'file_prefix': str(self.file_prefix),
            'parameter_file': self.parameter_file,
            'pin_in': self.pin_in,
            'out_dir': self.tool_wd
        }

    # /AWSTaskBase Interface

    # Luigi Interface
    def output(self):
        out_dir = self.tool_wd
        return {'percolator_pout_xml': s3.S3Target(posixpath.join(out_dir, 'percolator.pout.xml')),
                'percolator_params_txt': s3.S3Target(posixpath.join(out_dir, 'percolator.params.txt')),
                'percolator_log_txt': s3.S3Target(posixpath.join(out_dir, 'percolator.log.txt')),
                }
    # /Luigi Interface
