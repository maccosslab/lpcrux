import luigi
from luigi.util import inherits

from abstract import MSConvertParams, CometParams
from abstract import MakePinBase
from abstract import RunId
from buildtooldirectory import BuildToolDirectory
from comet import Comet


@inherits(RunId)
@inherits(MSConvertParams)
@inherits(CometParams)
class MakePin(MakePinBase):
    ms_files = luigi.ListParameter()

    @property
    def tool_wd(self):
        return self.input()['tool_wd'].path

    @property
    def out_pin(self):
        return self.output()['out_pin'].path

    @property
    def in_pins(self):
        return [self.input()[key]['comet_target_pin'].path for key in self.input() if key.startswith('comet_')]

    def requires(self):
        req = {
            'tool_wd': self.clone(BuildToolDirectory, toolname='make_pin')
        }
        for i, ms_file in enumerate(self.ms_files):
            req['comet_{}'.format(i)] = self.clone(Comet, ms_file=ms_file)

        return req

    @property
    def downloadable_outputs(self):
        return []
