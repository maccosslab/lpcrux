import ConfigParser
import os
import posixpath
import subprocess
import sys
import unittest
from subprocess import check_output, STDOUT, CalledProcessError

from lakitu.aws.s3helpers import search_s3_prefix
from lakitu.cli.cli_helpers import find_config_location
from lakituapi.pipeline_executor import PipelineExecutor

try:
    from lakitu.aws.s3helpers import s3_md5sum
except ImportError:
    from lakitu.aws.s3helpers import bs3, bucket_and_key_from_s3

    def s3_md5sum(s3_path):
        """
        Get md5 sum (or a modified md5 sum for large files uploaded with multipart upload (see https://stackoverflow.com/questions/6591047/etag-definition-changed-in-amazon-s3/28877788#28877788)
        :param s3_path:
        :return:
        """
        bucket, key = bucket_and_key_from_s3(s3_path)
        return bs3.head_object(
            Bucket=bucket,
            Key=key
        )['ETag'][1:-1]


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


class TestOnLakituConfiguredMachine(unittest.TestCase):
    """
    This test case relies on there being a ~/.lakitu directory that has been configured with an existing cluster using
     the lakitu command-line interface.
    """

    longMessage = True

    def setUp(self):
        # Start luigid. If luigid is running, a duplicate instance will not be created.
        self.luigid_out_log = open('luigid_out.log', 'w')  # create an output file for checking the results of luigid
        conda_path = os.path.dirname(os.path.realpath(sys.executable))  # add the current python binary path to our path to bypass needing to source the current conda environment
        my_env = os.environ.copy()
        my_env["PATH"] = conda_path + ":" + my_env["PATH"]
        self.luigid_process = subprocess.Popen(['luigid'], stdout=self.luigid_out_log, stderr=self.luigid_out_log, env=my_env)

        # Get environment variables for execution environment
        config = ConfigParser.RawConfigParser()
        config.read(find_config_location())
        config.read(config.get('aws', 'clusters_cfg'))
        self.my_env = PipelineExecutor(config=config).execution_environment

    def test_smoke(self):
        # Running smoke test
        cmds = [sys.executable, "../interface.py", "--workers", "100", "MainTask"]
        pipeline_args = ["--run-id", "smoke_test",
                         "--ms-set", "s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/msfiles.txt",
                         "--msconvert-config", "s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/msconvert_config.crux.txt",
                         "--fasta-file", "s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/uniprot-human_no_tr.fasta",
                         "--parameter-file", "s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/jarrett_high_high.params.txt"]
        cmds.extend(pipeline_args)
        print('Running:\n{}'.format(' '.join(cmds)))
        try:
            command_out = check_output(cmds, stderr=STDOUT, env=self.my_env)
        except CalledProcessError as err:
            print(err.output)
            raise
        print(command_out)

        # Check expected output against S3 output
        ## List output files
        test_output_prefix = "s3://maccosslab-lakitudata/lpcrux/smoke_test/output/"
        expected_output_prefix = "s3://atkeller.lakitu.public/pipeline_tests/test_lpcrux/smoke/expected_output/"
        ran_at_least_once = False
        for expected_out_s3_path in search_s3_prefix(expected_output_prefix):
            if expected_out_s3_path.endswith('/'):
                continue
            ran_at_least_once = True
            test_out_s3_path = posixpath.join(test_output_prefix, remove_prefix(expected_out_s3_path, prefix=expected_output_prefix))
            self.assertEqual(s3_md5sum(test_out_s3_path), s3_md5sum(expected_out_s3_path), msg="md5sum({}) != md5sum({})".format(test_out_s3_path, expected_out_s3_path))
        self.assertTrue(ran_at_least_once, "The expected output S3 directory is empty")

    def tearDown(self):
        self.luigid_process.kill()
        self.luigid_out_log.close()


if __name__ == '__main__':
    unittest.main()
