import luigi
from lakitu.aws.awstask import AwsTaskBase
from luigi.tools import deps
from luigi.util import inherits

from abstract import RunId
from copyoutputs import CopyOutputs
from lpcrux.abstract import SpecificTaskParameter


@inherits(RunId)
class CopyAllToOutputS3Dir(luigi.WrapperTask):
    seed_task = SpecificTaskParameter()

    def requires(self):
        upstream = deps.find_deps(self.seed_task, None)
        for t in upstream:
            if not isinstance(t, AwsTaskBase):
                continue
            if len(t.downloadable_outputs) > 0:
                yield self.clone(CopyOutputs, task_object=t)


if __name__ == '__main__':
    luigi.run()
