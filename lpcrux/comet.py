import posixpath

from luigi.util import inherits

from abstract import CometBase
from abstract import MSConvertParams
from abstract import RunId
from buildtooldirectory import BuildToolDirectory
from lpcrux.abstract import S3PathParameter
from msconvert import MSConvert
from uploadinputfiletos3 import UploadInputFileToS3


@inherits(RunId)
@inherits(MSConvertParams)
class Comet(CometBase):
    ms_file = S3PathParameter(description="S3 path to Proteowizard-supported MS file")

    @property
    def tool_wd(self):
        return self.input()['tool_wd'].path

    @property
    def fasta_in(self):
        return self.input()['fasta_s3'].path

    @property
    def mzxml_in(self):
        return self.input()['mzxml_s3'].path

    def requires(self):
        msconvert_task = self.clone(MSConvert, msc_in_path=self.ms_file, msc_config=self.msconvert_config)
        return {"mzxml_s3": msconvert_task,
                "fasta_s3": self.clone(UploadInputFileToS3, source_file=self.fasta_file),
                "tool_wd": self.clone(BuildToolDirectory, toolname=posixpath.basename(msconvert_task.output().path) + ".comet")}

    @property
    def downloadable_outputs(self):
        return []
