import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from luigi.util import inherits

from abstract import RunId
from buildtooldirectory import BuildToolDirectory
from buildworkingdirectory import BuildWorkingDirectory
from lpcrux.abstract import SpecificTaskParameter


@inherits(RunId)
class CopyOutputs(luigi.Task):
    task_object = SpecificTaskParameter()

    def requires(self):
        return {'task': self.task_object,
                'output_path': self.clone(BuildToolDirectory, toolname='output'),
                'base_directory': self.clone(BuildWorkingDirectory)}

    def run(self):
        for s3_path, s3_target in self.output().iteritems():
            s3helpers.cp_s3(s3_path, s3_target.path)

    def output(self):
        # map the s3 locations to s3 file paths, removing suffix of s3 base dir
        cp_requests = dict()
        for s3_target in self.requires()['task'].downloadable_outputs:
            s3_path = s3_target.path
            path_suffix = os.path.relpath(s3_path, start=self.input()['base_directory'].path)
            out_path = os.path.join(self.input()['output_path'].path, path_suffix)
            cp_requests[s3_path] = s3.S3Target(out_path)
        return cp_requests


if __name__ == '__main__':
    luigi.run()
